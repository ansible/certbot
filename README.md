# Certbot

> This role depends on the Ansible Galaxy collection `community.general`.
> That means you must install that collection in your playbook for this role to run!
> See below.


Certbot has [deprecated their PPA](https://launchpad.net/~certbot/+archive/ubuntu/certbot)
and [recommend installing from snapd](https://askubuntu.com/a/1278945/749455).


Install certbot using snap:
```
snap install certbot --classic
# Update $PATH in current session (login and logout is required otherwise)
hash -r
```

+ https://eff-certbot.readthedocs.io/en/stable/install.html#system-requirements
+ https://github.com/certbot/certbot/issues/7950
+ https://askubuntu.com/questions/952137/what-does-hash-r-command-do
+ https://www.jeffgeerling.com/blog/2020/ansible-best-practices-using-project-local-collections-and-roles



## Ansible collection `community.general.snap`

In your playbook repo, install the collection:
```
$ ansible-galaxy collection install community.general
```

To use it in a playbook, specify: `community.general.snap`.

+ https://docs.ansible.com/ansible/latest/collections/community/general/snap_module.html
